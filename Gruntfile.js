module.exports = function (grunt) {

    'use strict';

    var SRC          = './source/';
    var BUILD        = './build/';

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        autoprefixer: {
            options: {
                browsers: ['last 2 versions', '> 1%', 'ios 4', 'ff 17', 'ie 8', 'ie 7']
            },
            all: {
                files: [{
                    expand : true,
                    src    : ['sass/**.css'],
                    cwd    : SRC,
                    dest   : SRC,
                    ext    : '.css'
                }]
            }
        },

        clean: {
            all: [BUILD]
        },

        copy: {
            options: {
                processContentExclude: ['.DS_Store', '.gitignore', '.sass-cache', 'node_modules']
            },
            fonts: {
                files: [
                    {
                        cwd    : SRC,
                        dest   : BUILD,
                        src    : ['fonts/**/*.{eot,otf,svg,ttf,woff}'],
                        expand : true,
                        filter : 'isFile'
                    }
                ]
            },
            html: {
                files: [
                    {
                        cwd    : SRC,
                        dest   : BUILD,
                        src    : ['**/*.html'],
                        expand : true,
                        filter : 'isFile'
                    }
                ]
            },
            images: {
                files: [
                    {
                        cwd    : SRC,
                        dest   : BUILD,
                        src    : ['images/*.{gif,jpg,png}'],
                        expand : true,
                        filter : 'isFile'
                    }
                ]
            },
            scripts: {
                files: [
                    {
                        cwd     : SRC,
                        dest    : BUILD,
                        src     : ['js/**/*.js'],
                        expand  : true,
                        filter  : 'isFile'
                    }
                ]
            },
            styles: {
                files: [
                    {
                        cwd     : SRC,
                        dest    : BUILD,
                        src     : ['css/**'],
                        expand  : true,
                        filter  : 'isFile'
                    }
                ]
            }
        },

        jshint: {
            options: {
                browser     : true,
                curly       : true,
                devel       : true,
                eqeqeq      : true,
                evil        : true,
                immed       : true,
                regexdash   : true,
                asi         : true,
                sub         : true,
                trailing    : true,
                globals     : {
                               angular    : true,
                               app        : true,
                               jQuery     : true,
                               modernizr  : true
                              },
                force       : true
            },
            dev: {
                src: [
                    SRC + 'js/**/*.js',
                    '!' + SRC + 'js/vendor/**/*.js'
                ]
            },
            gruntfile: {
                src: ['Gruntfile.js']
            }
        },

        sass: {
            options: {
                sourcemap  : false,
                trace      : true
            },
            dev: {
                options: {
                    style  : 'expanded'
                },
                files: [{
                    expand : true,
                    src    : ['**/*.scss', '**/modules/_*.scss', '!**/_*.scss'],
                    cwd    : SRC + 'sass',
                    dest   : SRC + 'css',
                    ext    : '.css'
                }]
            },
            prod: {
                options: {
                    style  : 'compressed'
                },
                files: [{
                    expand : true,
                    src    : ['**/*.scss', '**/modules/_*.scss', '!**/_*.scss', '!**/*.css.map'],
                    cwd    : SRC + 'sass',
                    dest   : SRC + 'css',
                    ext    : '.css'
                }]
            }
        },

        scriptlinker: {
            options: {
                fileTmpl     : '<script src="%s"></script>',
                appRoot      : BUILD
            },
            modernizr: {
                options: {
                    startTag : '<!--MODERNIZR-->',
                    endTag   : '<!--MODERNIZR END-->'
                },
                files: {
                    'build/index.html': [
                        BUILD + 'js/vendor/modernizr.js'
                    ]
                }
            },
            scripts: {
                options: {
                    startTag : '<!--SCRIPTS-->',
                    endTag   : '<!--SCRIPTS END-->'
                },
                files: {
                    'build/index.html': [
                        BUILD + 'js/vendor/firebase.js',
                        BUILD + 'js/vendor/angular.js',
                        BUILD + 'js/vendor/angularfire.js',
                        BUILD + 'js/*.js',
                        BUILD + 'js/modules/**/*.js',
                        BUILD + 'js/controllers/**/*.js',
                        BUILD + 'js/directives/**/*.js',
                        BUILD + 'js/factories/**/*.js',
                        BUILD + 'js/views/**/*.js'
                    ]
                }
            }
        },

        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd | hh:MM") %> */\n',
                mangle: true
            },
            build: {
                files: {
                    'build/js/vendor/modernizr.js': [
                        SRC + 'js/vendor/modernizr.js'
                    ],
                    'build/js/script.js': [
                        SRC + 'js/vendor/firebase.js',
                        SRC + 'js/vendor/angular.js',
                        SRC + 'js/vendor/angularfire.js',
                        SRC + 'js/*.js',
                        SRC + 'js/modules/**/*.js',
                        SRC + 'js/controllers/**/*.js',
                        SRC + 'js/directives/**/*.js',
                        SRC + 'js/factories/**/*.js',
                        SRC + 'js/views/**/*.js'
                    ]
                }
            }
        },

        pngmin: {
            build: {
                options: {
                    force      : true,
                    ext        : '.png'
                },
                files: [{
                        expand : true,
                        src    : ['**/*.png'],
                        cwd    : BUILD + 'images',
                        dest   : BUILD + 'images'
                    }
                ]
            }
        },

        watch: {
            fonts: {
                expand : true,
                files  : [SRC + 'fonts/**'],
                tasks  : ['copy:fonts']
            },
            gruntfile: {
                expand : true,
                files  : 'Gruntfile.js',
                tasks  : ['jshint:gruntfile', 'clean', 'jshint', 'sass:dev', 'autoprefixer', 'copy']
            },
            html: {
                expand : true,
                files  : SRC + '**/*.html',
                tasks  : ['copy:html', 'scriptlinker']
            },
            images: {
                expand : true,
                files  : [SRC + 'images/*.{gif,jpg,png}'],
                tasks  : ['copy:images']
            },
            sass: {
                expand : true,
                files  : [SRC + '**/*.scss'],
                tasks  : ['sass:dev', 'autoprefixer', 'copy:styles']
            },
            scripts: {
                expand : true,
                files  : [SRC + 'js/**/*.js'],
                tasks  : ['jshint', 'copy:scripts', 'scriptlinker']
            }
        }

    });

    require('matchdep').filter('grunt-*').forEach(grunt.loadNpmTasks);
    grunt.loadNpmTasks('grunt-notify');

    grunt.registerTask('dev', ['clean', 'jshint', 'sass:dev', 'autoprefixer', 'copy', 'scriptlinker', 'watch']);
    
    grunt.registerTask('build', ['clean', 'jshint', 'sass:prod', 'autoprefixer', 'copy:fonts', 'copy:html', 'copy:images', 'copy:styles', 'uglify', 'pngmin', 'scriptlinker', 'watch']);

    grunt.registerTask('default', ['build']);
};
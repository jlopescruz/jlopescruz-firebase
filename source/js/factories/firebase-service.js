app.factory('firebaseService', ['$firebase', function($firebase) {
    'use strict';

    return {
        
        getProjectById : function (id) {
            return $firebase(new Firebase('https://jlopescruz.firebaseio.com/projects/' + id));
        },

        getProjects : function () {
            return $firebase(new Firebase('https://jlopescruz.firebaseio.com/projects'));
        },
        
        getPropertyById : function (property, id) {
            return $firebase(new Firebase('https://jlopescruz.firebaseio.com/' + property + '/' + id));
        },
        
        getAll : function () {
            return $firebase(new Firebase('https://jlopescruz.firebaseio.com/'));
        }
    }
}]);
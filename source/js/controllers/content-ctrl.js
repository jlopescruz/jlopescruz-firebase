app.controller('contentCtrl', ['$scope', 'firebaseService', function($scope, firebaseService) {
    
    $scope.message = 'This is the content';
    $scope.items = firebaseService.getProjects();
    
}]);
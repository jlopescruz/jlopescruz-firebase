app.controller('demoCtrl', ['$scope', 'firebaseService', function($scope, firebaseService) {
    $scope.demoValue = 'hey!';
    $scope.projects = firebaseService.getProjects();
    $scope.specificProject = firebaseService.getProjectById(3);
}]);
app.directive('footer', function() {
    'use strict';

    return {
        controller: 'footerCtrl',
        restrict: 'A',
        scope: {
            demo: '='
        },
        replace: false,
        templateUrl: 'js/views/footer.html'
    };
});
app.directive('header', function() {
    'use strict';

    return {
        controller: 'headerCtrl',
        restrict: 'A',
        scope: {
            demo: '='
        },
        replace: false,
        templateUrl: 'js/views/header.html'
    };
});
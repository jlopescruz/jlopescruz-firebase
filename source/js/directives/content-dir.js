app.directive('content', function() {
    'use strict';

    return {
        controller: 'contentCtrl',
        restrict: 'A',
        scope: {
            demo: '='
        },
        replace: false,
        templateUrl: 'js/views/content.html'
    };
});
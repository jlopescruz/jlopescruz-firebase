app.directive('demo', function() {
    'use strict';

    return {
        controller: 'demoCtrl',
        restrict: 'A',
        scope: {
            demo: '='
        },
        replace: false,
        templateUrl: 'js/views/demo-view.html'
    };
});